package com.youngcms.core.queue.command;

public abstract class BaseCommand{
	
	public abstract void execute(String params);
	
}
