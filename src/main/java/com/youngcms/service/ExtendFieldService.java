package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.ExtendField;
import com.youngcms.dao.ExtendFieldMapper;
@Service
public class ExtendFieldService extends ServiceImpl<ExtendFieldMapper,ExtendField> {
	
}
