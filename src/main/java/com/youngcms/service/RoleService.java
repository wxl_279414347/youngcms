package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.Role;
import com.youngcms.dao.RoleMapper;
@Service
public class RoleService extends ServiceImpl<RoleMapper,Role> {
}
