package com.youngcms.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.ReptileRule;
import com.youngcms.dao.ReptileRuleMapper;
@Service
public class ReptileRuleService extends ServiceImpl<ReptileRuleMapper,ReptileRule> {

}
