package com.youngcms.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.AdvertPosition;
import com.youngcms.dao.AdvertPositionMapper;

@Service
public class AdvertPositionService extends ServiceImpl<AdvertPositionMapper, AdvertPosition> {

}
