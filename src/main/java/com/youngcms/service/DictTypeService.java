package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.DictType;
import com.youngcms.dao.DictTypeMapper;
@Service
public class DictTypeService extends ServiceImpl<DictTypeMapper,DictType> {
}
