package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.ExtendFieldValue;
import com.youngcms.dao.ExtendFieldValueMapper;
@Service
public class ExtendFieldValueService extends ServiceImpl<ExtendFieldValueMapper,ExtendFieldValue> {
	
}
