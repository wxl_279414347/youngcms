package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.ContentVideo;

public interface ContentVideoMapper extends BaseMapper<ContentVideo> {

}