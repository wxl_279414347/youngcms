package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}