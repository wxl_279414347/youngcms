package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.FriendLink;

public interface FriendLinkMapper extends BaseMapper<FriendLink> {
}