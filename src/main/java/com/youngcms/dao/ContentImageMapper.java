package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.ContentImage;

public interface ContentImageMapper extends BaseMapper<ContentImage> {

}