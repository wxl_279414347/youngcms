package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.DictType;

public interface DictTypeMapper extends BaseMapper<DictType> {
	
}