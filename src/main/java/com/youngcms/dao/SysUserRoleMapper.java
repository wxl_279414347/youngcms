package com.youngcms.dao;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

	void deleteBySysUserId(@Param("sysUserId")Integer sysUserId);

	
}