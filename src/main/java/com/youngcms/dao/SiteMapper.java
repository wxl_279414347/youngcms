package com.youngcms.dao;

import com.youngcms.bean.Site;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author fumiao
 * @since 2017-07-29
 */
public interface SiteMapper extends BaseMapper<Site> {

}