package com.youngcms.dao;

import com.youngcms.bean.ExtendFieldValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author fumiao
 * @since 2017-07-31
 */
public interface ExtendFieldValueMapper extends BaseMapper<ExtendFieldValue> {

}