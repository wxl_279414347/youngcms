package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.Flow;

public interface FlowMapper extends BaseMapper<Flow> {
}