package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.FlowFace;

public interface FlowFaceMapper extends BaseMapper<FlowFace> {
}