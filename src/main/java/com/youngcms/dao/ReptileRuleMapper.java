package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.ReptileRule;

public interface ReptileRuleMapper extends BaseMapper<ReptileRule> {
}