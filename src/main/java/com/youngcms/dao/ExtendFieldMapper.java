package com.youngcms.dao;

import com.youngcms.bean.ExtendField;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author fumiao
 * @since 2017-07-28
 */
public interface ExtendFieldMapper extends BaseMapper<ExtendField> {

}